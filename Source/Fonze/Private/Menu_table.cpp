// Fill out your copyright notice in the Description page of Project Settings.

#include "Fonze.h"
#include "Menu_table.h"


// Sets default values
AMenu_table::AMenu_table()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMenu_table::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMenu_table::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void AMenu_table::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

