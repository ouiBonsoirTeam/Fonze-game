// Fill out your copyright notice in the Description page of Project Settings.

#include "Fonze.h"
#include "CameraLookAt.h"


// Sets default values
ACameraLookAt::ACameraLookAt()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Init Default parameters
	springArmLocation = FVector(0.f, 0.f, 50.f);
	springArmRotation = FRotator(-80.f, 0.f, 0.f);
	minArmDistance = 600.f;
	maxArmDistance = 1000.f;


	// Create our components
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	FVector2D root_pos = FVector2D(RootComponent->RelativeLocation);
	RootComponent->SetRelativeLocation(FVector(root_pos, 0.f));
	m_cameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	m_cameraSpringArm->AttachTo(RootComponent);
	m_cameraSpringArm->SetRelativeLocationAndRotation(springArmLocation, springArmRotation);
	m_cameraSpringArm->TargetArmLength = minArmDistance;
	m_cameraSpringArm->bEnableCameraLag = true;
	m_cameraSpringArm->CameraLagSpeed = 2.0f;

	// Attach camera spring arm to camera
	m_camera = CreateDefaultSubobject<UCameraComponent>(TEXT("GameCamera"));
	m_camera->AttachTo(m_cameraSpringArm, USpringArmComponent::SocketName);
	m_camera->bConstrainAspectRatio = true;
	m_camera->AspectRatio = 1.777;

	m_cameraSpringArm->bDoCollisionTest = false;

	// Invisible Walls
	top_wall = CreateDefaultSubobject<UBoxComponent>(TEXT("Top Wall"));
	top_wall->AttachTo(RootComponent);
	top_wall->SetRelativeLocation(FVector(900, 0, 0));
	top_wall->SetBoxExtent(FVector(100, 1400, 1000));
	top_wall->SetCollisionProfileName(FName("cam_walls"));
	
	bottom_wall = CreateDefaultSubobject<UBoxComponent>(TEXT("Bottom Wall"));
	bottom_wall->AttachTo(RootComponent);
	bottom_wall->SetRelativeLocation(FVector(-900, 0, 0));
	bottom_wall->SetBoxExtent(FVector(100, 1400, 1000));
	bottom_wall->SetCollisionProfileName(FName("cam_walls"));
	
	right_wall = CreateDefaultSubobject<UBoxComponent>(TEXT("Right Wall"));
	right_wall->AttachTo(RootComponent);
	right_wall->SetRelativeLocation(FVector(0, 1400, 0));
	right_wall->SetBoxExtent(FVector(800, 100, 1000));
	right_wall->SetCollisionProfileName(FName("cam_walls"));
	
	left_wall = CreateDefaultSubobject<UBoxComponent>(TEXT("Left Wall"));
	left_wall->AttachTo(RootComponent);
	left_wall->SetRelativeLocation(FVector(0, -1400, 0));
	left_wall->SetBoxExtent(FVector(800, 100, 1000));
	left_wall->SetCollisionProfileName(FName("cam_walls"));

	// Overlaps

	// Invisible Walls
	top_over = CreateDefaultSubobject<UBoxComponent>(TEXT("Top"));
	top_over->AttachTo(RootComponent);
	top_over->SetRelativeLocation(FVector(780, 0, 0));
	top_over->SetBoxExtent(FVector(20, 1300, 1000));

	bottom_over = CreateDefaultSubobject<UBoxComponent>(TEXT("Bottom"));
	bottom_over->AttachTo(RootComponent);
	bottom_over->SetRelativeLocation(FVector(-780, 0, 0));
	bottom_over->SetBoxExtent(FVector(20, 1300, 1000));

	right_over = CreateDefaultSubobject<UBoxComponent>(TEXT("Right"));
	right_over->AttachTo(RootComponent);
	right_over->SetRelativeLocation(FVector(0, 1280, 0));
	right_over->SetBoxExtent(FVector(760, 50, 1000));

	left_over = CreateDefaultSubobject<UBoxComponent>(TEXT("Left"));
	left_over->AttachTo(RootComponent);
	left_over->SetRelativeLocation(FVector(0, -1280, 0));
	left_over->SetBoxExtent(FVector(760, 50, 1000));
}

// Called when the game starts or when spawned
void ACameraLookAt::BeginPlay()
{
	Super::BeginPlay();

	// Initialize parameters with values set (or not) in Editor
	m_cameraSpringArm->SetRelativeLocationAndRotation(springArmLocation, springArmRotation);
	m_cameraSpringArm->TargetArmLength = minArmDistance;

	for (TActorIterator<AHero> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		m_heroesAndVehicles.Add(*ActorItr);
	}

	for (TActorIterator<AWheeledVehicle> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		m_heroesAndVehicles.Add(*ActorItr);
	}
}

// Called every frame
void ACameraLookAt::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	float distance = 0.f;
	float vert_distance = 0.f;
	FVector targetPosition;

	bool overlapped = false;

	for (size_t i = 0; i < m_heroesAndVehicles.Num(); ++i)
	{

		if (m_heroesAndVehicles[i]->IsA(AHero::StaticClass()) && static_cast<AHero*>(m_heroesAndVehicles[i])->m_isDead){
			continue;
		}
		
		for (size_t j = i + 1; j < m_heroesAndVehicles.Num(); j++)
		{
			if (m_heroesAndVehicles[j]->IsA(AHero::StaticClass()) && static_cast<AHero*>(m_heroesAndVehicles[j])->m_isDead){
				continue;
			}
			
			if (m_heroesAndVehicles[i]->IsControlled() && m_heroesAndVehicles[j]->IsControlled())
			{
				overlapped = overlapped || top_over->IsOverlappingActor(m_heroesAndVehicles[i]);
				overlapped = overlapped || bottom_over->IsOverlappingActor(m_heroesAndVehicles[i]);
				overlapped = overlapped || right_over->IsOverlappingActor(m_heroesAndVehicles[i]);
				overlapped = overlapped || left_over->IsOverlappingActor(m_heroesAndVehicles[i]);
				
				float temp_dist = FVector::Dist(m_heroesAndVehicles[i]->GetActorLocation(), m_heroesAndVehicles[j]->GetActorLocation());
				float temp_vert_dist = FMath::Abs(m_heroesAndVehicles[i]->GetActorLocation().X - m_heroesAndVehicles[j]->GetActorLocation().X) + FMath::Abs(m_heroesAndVehicles[i]->GetActorLocation().Y - m_heroesAndVehicles[j]->GetActorLocation().Y);

				if (temp_dist > distance)
				{
					distance = temp_dist;
				}
				if (temp_vert_dist > vert_distance)
				{
					vert_distance = temp_vert_dist;
					targetPosition = m_heroesAndVehicles[j]->GetActorLocation() + (m_heroesAndVehicles[i]->GetActorLocation() - m_heroesAndVehicles[j]->GetActorLocation()) / 2;
				}

			}
		}
	}

	if (!overlapped)
		SetActorLocation(targetPosition);
	m_cameraSpringArm->TargetArmLength = FMath::Clamp(distance, minArmDistance, maxArmDistance) * 1.5f;

}
