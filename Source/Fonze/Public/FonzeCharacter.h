// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "FonzeCharacter.generated.h"

UCLASS()
class FONZE_API AFonzeCharacter : public ACharacter
{
	GENERATED_BODY()

public:

	//Health
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "FonzeCharacter")
	float m_health;

	//Dead
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "FonzeCharacter")
	bool m_isDead;

	// Sets default values for this character's properties
	AFonzeCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	//Death function (helper)
	virtual void CalculateDead();

	//Health function
	UFUNCTION(BlueprintCallable, Category = "FonzeCharacter")
	virtual void CalculateHealth(float delta);

#if WITH_EDITOR
	//Editor
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif
	
	
};
