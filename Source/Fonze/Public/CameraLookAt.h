// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "GameFramework/Pawn.h"
#include "Hero.h"
#include "CameraLookAt.generated.h"

UCLASS()
class FONZE_API ACameraLookAt : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	USpringArmComponent* m_cameraSpringArm;
	UPROPERTY(EditAnywhere)
	UCameraComponent* m_camera;

	TArray<APawn*> m_heroesAndVehicles;

	UPROPERTY(EditAnywhere)
	FVector springArmLocation;
	UPROPERTY(EditAnywhere)
	FRotator springArmRotation;

	UPROPERTY(EditAnywhere)
	float minArmDistance;
	UPROPERTY(EditAnywhere)
	float maxArmDistance;

	// Invisibles walls
	UPROPERTY(EditAnywhere)
	UBoxComponent* top_wall;
	UPROPERTY(EditAnywhere)
	UBoxComponent* bottom_wall;
	UPROPERTY(EditAnywhere)
	UBoxComponent* right_wall;
	UPROPERTY(EditAnywhere)
	UBoxComponent* left_wall;

	// Invisibles overlaps
	UPROPERTY(EditAnywhere)
	UBoxComponent* top_over;
	UPROPERTY(EditAnywhere)
	UBoxComponent* bottom_over;
	UPROPERTY(EditAnywhere)
	UBoxComponent* right_over;
	UPROPERTY(EditAnywhere)
	UBoxComponent* left_over;

public:
	// Sets default values for this pawn's properties
	ACameraLookAt();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
	
};
