// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Menu_table.generated.h"


/** Structure that defines a characters table entry */
USTRUCT(BlueprintType)
struct FCharactersData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	FCharactersData()
	{}


	/** Name of the character */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PropCharacter)
		FText Name;

	/** Property of the character which is shown in the menu when you choose your character */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PropCharacter)
		FText Prop;

};


/** Structure that defines a weapon table entry */
USTRUCT(BlueprintType)
struct FWeaponsData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	FWeaponsData()
	{}


	/** Name of the weapon */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = NameWeapon)
		FText Name;

	/** Property of the weapon which is shown in the menu when you choose your character */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PropWeapon)
		FText Prop;


};


UCLASS()
class FONZE_API AMenu_table : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMenu_table();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	
	
};


